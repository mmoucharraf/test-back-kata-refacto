<?php

class TemplateManager
{
    //Mettre les informations relatives au voyage dans des variables internes

    private $_quoteFromRepository;
    private $usefulObject;
    private $destination;
    private $destinationOfQuote;

    private $needles = [
        ['[quote:destination_name]',  'setDestinationName'],
        ['[quote:summary_html]',  'setSummaryHtml'],
        ['[quote:summary]', 'setSummary'],
        ['[user:first_name]',  'setFirstName'],
        ['[quote:destination_link]', 'setDestinationLink', 'setDestinationLinkDefault']
    ];


    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new \RuntimeException('no tpl given');
        }

        $replaced = clone($tpl);
        $this->init($data);
        $replaced->subject = $this->computeText($replaced->subject, $data);
        $replaced->content = $this->computeText($replaced->content, $data);

        return $replaced;
    }

    /*   Initialisation des variables internes de la classe  */

    private function init($text,array $data)
    {
        $quote = (isset($data['quote']) and $data['quote'] instanceof Quote) ? $data['quote'] : null;

        if ($quote)
        {
            $this->_quoteFromRepository = QuoteRepository::getInstance()->getById($quote->id);
            $this->usefulObject = SiteRepository::getInstance()->getById($quote->siteId);
            $this->destinationOfQuote = DestinationRepository::getInstance()->getById($quote->destinationId);

            $this->destination = DestinationRepository::getInstance()->getById($quote->destinationId);

        }
    }

    private function computeText($text, array $data)
    {
        for ($i = 0; $i < count($this->needles); $i++) {
                // on verifie si le mot cherché  est dans le texte
            if (strpos($text, $this->needles[$i]['0'])) {
                //Si oui, on appelle la methode de remplacement
                $this->{$this->needles[$i]['1']}($text, $this->needles[$i]['0']);
            } else if (array_key_exists('2', $this->needles[$i])) {
                //Si non, on appelle la methode par defaut, si elle est definie
                $this->{$this->replacements[$i]['2']}($text);
            }
            return $text;
        }
    }

    private function setSummaryHtml(&$text, $needle)
    {
        $text = str_replace($needle, Quote::renderHtml($this->_quoteFromRepository), $text);
    }

    private function setSummary(&$text, $needle)
    {
        $text = str_replace($needle, Quote::renderText($this->_quoteFromRepository), $text);
    }

    private function setDestinationName(&$text, $needle)
    {
        $text = str_replace($needle, $this->destinationOfQuote->countryName,$text);
    }
    private function setFirstName(&$text, $needle)
    {
        $_user = (isset($data['user'])  and ($data['user']  instanceof User))  ? $data['user']  : ApplicationContext::getInstance()->getCurrentUser();
        $text = str_replace($needle, ucfirst(mb_strtolower($_user->firstname)), $text);
    }

    private function setDestinationLink(&$text, $needle)
    {
        $text = str_replace('[quote:destination_link]', $this->usefulObject->url . '/' . $this->destination->countryName . '/quote/' . $this->_quoteFromRepository->id, $text);
    }
    private function setDestinationLinkDefault(&$text)
    {
        $text = str_replace('[quote:destination_link]', '', $text);
    }




}
